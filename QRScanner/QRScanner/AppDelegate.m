//
//  AppDelegate.m
//  QRScanner
//
//  Created by Raúl Blánquez on 22/09/14.
//  Copyright (c) 2014 Raul Blanquez. All rights reserved.
//

#import "AppDelegate.h"
#import <StartApp/StartApp.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#ifndef DEBUG
    //************* START APP INITIALIZATION ********************* //
    STAStartAppSDK* sdk = [STAStartAppSDK sharedInstance];
    sdk.appID = @"209141323";
    sdk.devID = @"103288010";
    
    STASplashPreferences *preferences = [[STASplashPreferences alloc] init];
    preferences.splashMode = STASplashModeTemplate;
    preferences.splashTemplateTheme = STASplashTemplateThemeAshenSky;
    preferences.splashTemplateIconImageName = @"qrcode.png";
    preferences.splashTemplateAppName = @"iQR Fast Scanner";
    preferences.splashLoadingIndicatorType = STASplashLoadingIndicatorTypeDots;
    [sdk showSplashAdWithPreferences:preferences];
    
#endif

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
