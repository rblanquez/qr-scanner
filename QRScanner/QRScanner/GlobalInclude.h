//
//  GlobalInclude.h
//  QRScanner
//
//  Created by Raúl Blánquez on 23/09/14.
//  Copyright (c) 2014 Raul Blanquez. All rights reserved.
//

#ifndef QRScanner_GlobalInclude_h
#define QRScanner_GlobalInclude_h

@import UIKit;

//Usar Delog para logs que solo tienen que estar en Debug
#ifdef DEBUG
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define DLog(...) {}
#endif

//Usar RLog para logs que tienen que estar en Release
#define RLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);


#endif
