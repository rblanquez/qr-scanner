//
//  QRManager.m
//  QRScanner
//
//  Created by Raúl Blánquez on 23/09/14.
//  Copyright (c) 2014 Raul Blanquez. All rights reserved.
//

#import "QRManager.h"
#import "GlobalInclude.h"

#define kQRPrefixMail   @"mailto:"
#define kQRPrefixMail2  @"smtp:"
#define kQRPrefixMail3  @"matmsg:to:"
#define kQRPrefixMAIL   @"mailto:"
#define kQRPrefixMAIL2  @"smtp:"
#define kQRPrefixMAIL3  @"matmsg:to:"

#define kQRPrefixSms    @"smsto:"
#define kQRPrefixSMS    @"SMSTO:"

#define kQRPrefixGeo    @"geo:"
#define kQRPrefixGEO    @"GEO:"

#define kQRPrefixTel    @"tel:"
#define kQRPrefixTEL    @"TEL:"

#define KQRPrefixUrl    @"http:"
#define kQRPrefixUrl2   @"https:"
#define KQRPrefixURL    @"HTTP:"
#define kQRPrefixURL2   @"HTTPS:"

#define kQRPrefixSMSContent @":SMS "

@implementation QRManager

+ (void)manageQRCode:(NSString *)qrString
{
    DLog(@"qrString = %@###", qrString);
    
    if ([self checkAction:KQRPrefixUrl inQrString:qrString]  || [self checkAction:KQRPrefixURL inQrString:qrString] ||
        [self checkAction:kQRPrefixUrl2 inQrString:qrString] || [self checkAction:kQRPrefixURL2 inQrString:qrString])
    {
        DLog(@"It's an URL");
        [self openUrlFromQRString:qrString];
        return;
    }
    
    if ([self checkAction:kQRPrefixTel inQrString:qrString] || [self checkAction:kQRPrefixTEL inQrString:qrString])
    {
        DLog(@"It's a phone call");
        [self callPhoneNumber:qrString];
        return;
    }
    
    if ([self checkAction:kQRPrefixMail inQrString:qrString]  || [self checkAction:kQRPrefixMAIL inQrString:qrString]  ||
        [self checkAction:kQRPrefixMail2 inQrString:qrString] || [self checkAction:kQRPrefixMAIL2 inQrString:qrString] ||
        [self checkAction:kQRPrefixMail3 inQrString:qrString] || [self checkAction:kQRPrefixMAIL3 inQrString:qrString])
    {
        DLog(@"It's an e-mail");
        [self sendMail:qrString];
        return;
    }
    
    if ([self checkAction:kQRPrefixSms inQrString:qrString] || [self checkAction:kQRPrefixSMS inQrString:qrString])
    {
        DLog(@"It's an SMS");
        [self sendSMS:qrString];
        return;
    }
    
    if ([self checkAction:kQRPrefixGeo inQrString:qrString] || [self checkAction:kQRPrefixGEO inQrString:qrString])
    {
        DLog(@"It's a map coordinate");
        [self openMaps:qrString];
        return;
    }
    
    //The default is a text message, but in some cases we get a web address without http
#warning Check url format
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"QR"
                                                    message:qrString
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    NSString *qrStringWithHttp = [NSString stringWithFormat:@"http://%@", qrString];
    [self openUrlFromQRString:qrStringWithHttp];
}

// *****************************************************************************
#pragma mark -                                                          Url
// *****************************************************************************
+ (void)openUrlFromQRString:(NSString *)qrString
{
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVXYZ01234567890$-_.+!*'(),:/"] invertedSet];
    NSString *sanitizedString = [[qrString componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
        
    NSURL *url = [NSURL URLWithString:sanitizedString];
    
    [[UIApplication sharedApplication] openURL:url];
}

// *****************************************************************************
#pragma mark -                                                          Call
// *****************************************************************************
+ (void)callPhoneNumber:(NSString *)qrString
{
    NSURL *phoneURL = [NSURL URLWithString:qrString];
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL])
    {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}

// *****************************************************************************
#pragma mark -                                                          Mail
// *****************************************************************************
+ (void)sendMail:(NSString *)qrString
{
    NSURL *mailURL = [NSURL URLWithString:qrString];
    if ([[UIApplication sharedApplication] canOpenURL:mailURL])
    {
        [[UIApplication sharedApplication] openURL:mailURL];
    }
}

+ (void)sendMail2:(NSString *)qrString
{
    NSRange prefixRange     = [qrString rangeOfString:kQRPrefixMail2];
    
    NSString *address   = [qrString substringFromIndex:prefixRange.length];

    NSURL *mailURL = [NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@", address]];
    if ([[UIApplication sharedApplication] canOpenURL:mailURL])
    {
        [[UIApplication sharedApplication] openURL:mailURL];
    }
}

+ (void)sendMail3:(NSString *)qrString
{
    NSRange prefixRange     = [qrString rangeOfString:kQRPrefixMail3];
    
    NSString *address   = [qrString substringFromIndex:prefixRange.length];
    
    NSRange subRange    = [address rangeOfString:@"SUB:"];
    
    if (subRange.location != NSNotFound)
    {
        address = [address substringWithRange:NSMakeRange(0, subRange.location)];
    }
    
    NSURL *mailURL = [NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@", address]];
    if ([[UIApplication sharedApplication] canOpenURL:mailURL])
    {
        [[UIApplication sharedApplication] openURL:mailURL];
    }
}

// *****************************************************************************
#pragma mark -                                                          SMS
// *****************************************************************************
+ (void)sendSMS:(NSString *)qrString
{
    NSRange prefixRange     = [qrString rangeOfString:kQRPrefixSMS];
    
    NSString *numberAndBodyString = [qrString substringFromIndex:prefixRange.length];
    
    NSRange bodyRange      = [numberAndBodyString rangeOfString:@":"];
    
    if (bodyRange.location != NSNotFound)
    {
        NSString *numberString  = [numberAndBodyString substringToIndex:bodyRange.location];
        
        //NSString *smsString = [NSString stringWithFormat:@"sms:%@?body=hola", numberString];
        NSString *smsString = [NSString stringWithFormat:@"sms:%@", numberString];
        
        NSURL *smsURL = [NSURL URLWithString:smsString];
        if ([[UIApplication sharedApplication] canOpenURL:smsURL])
        {
            [[UIApplication sharedApplication] openURL:smsURL];
        }
    }
}

// *****************************************************************************
#pragma mark -                                                          Maps
// *****************************************************************************
+ (void)openMaps:(NSString *)qrString
{
    NSRange prefixRange =[qrString rangeOfString:kQRPrefixGeo];
    
    NSString *coordsstring = [qrString substringFromIndex:prefixRange.length];
    
    //NSString *mapString = [NSString stringWithFormat:@"http://maps.apple.com/?q=48.8586,2.34.82"];
    NSString *mapString = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@", coordsstring];

    NSURL *mapURL = [NSURL URLWithString:mapString];
    if ([[UIApplication sharedApplication] canOpenURL:mapURL])
    {
        [[UIApplication sharedApplication] openURL:mapURL];
    }
}

+ (BOOL)checkAction:(NSString *)action inQrString:(NSString *)qrString
{
    NSRange range = [qrString rangeOfString:action];
    
    return range.location == 0;
}

@end
