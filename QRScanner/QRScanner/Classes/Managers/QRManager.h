//
//  QRManager.h
//  QRScanner
//
//  Created by Raúl Blánquez on 23/09/14.
//  Copyright (c) 2014 Raul Blanquez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QRManager : NSObject

+ (void)manageQRCode:(NSString *)qrString;

@end
