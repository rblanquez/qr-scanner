//
//  QRHelpVC.m
//  QRScanner
//
//  Created by Raúl Blánquez on 25/09/14.
//  Copyright (c) 2014 Raul Blanquez. All rights reserved.
//

#import "QRHelpVC.h"
#import "GlobalInclude.h"

@implementation QRHelpVC

// *****************************************************************************
#pragma mark -                                          ViewController Lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    DLog();
    
    [super viewDidLoad];
    
    [self _initData];
    [self _localizeLabels];
}

// *****************************************************************************
#pragma mark -                                          Target actions
// *****************************************************************************
- (IBAction)closeHelp:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:Nil];
}

// *****************************************************************************
#pragma mark -                                          Private Methods
// *****************************************************************************
- (void)_initData
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Help" ofType:@"plist"];
    NSDictionary *helpDict = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    self.helpBody.text = helpDict[@"Body"];
}

- (void)_localizeLabels
{
    self.helpTitle.text = NSLocalizedString(@"helpTitle", Nil);
}

@end
