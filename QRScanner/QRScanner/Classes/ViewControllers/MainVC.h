//
//  MainVC.h
//  QRScanner
//
//  Created by Raúl Blánquez on 23/09/14.
//  Copyright (c) 2014 Raul Blanquez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QRScannerVC.h"

@interface MainVC : UIViewController <QRScannerVCDelegate>

@end
