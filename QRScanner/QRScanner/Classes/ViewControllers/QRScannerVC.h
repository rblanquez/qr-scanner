//
//  QRScannerVC.h
//  QRScanner
//
//  Created by Raúl Blánquez on 22/09/14.
//  Copyright (c) 2014 Raul Blanquez. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QRScannerVCDelegate <NSObject>

- (void)qrScannerDidFinish;
- (void)qrScannerDidCancel;

@end

@interface QRScannerVC : UIViewController

@property (nonatomic, assign) id<QRScannerVCDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIView *shadowLayer;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end
