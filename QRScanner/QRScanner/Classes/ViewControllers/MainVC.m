//
//  MainVC.m
//  QRScanner
//
//  Created by Raúl Blánquez on 23/09/14.
//  Copyright (c) 2014 Raul Blanquez. All rights reserved.
//

#import "GlobalInclude.h"
#import "MainVC.h"
#import <StartApp/StartApp.h>

@interface MainVC ()
{
    STABannerView   *_bannerView;
    STAStartAppAd   *_startAppAd;
}

@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UILabel *appTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *pressToScanButton;
@property (weak, nonatomic) IBOutlet UIButton *helpButton;
@end

@implementation MainVC

// *****************************************************************************
#pragma mark -                                          ViewController Lifecycle
// *****************************************************************************
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self showStartAppBanner];
}

- (void)viewDidLoad
{
    DLog();
    
    [super viewDidLoad];
    
    [self localizeLabels];
    
    [self inicializarBannerAd];
}

- (void)viewDidAppear:(BOOL)animated
{
    DLog();
    
    [super viewDidAppear:animated];
}

- (void)viewDidLayoutSubviews
{
    DLog();
    
    [super viewDidLayoutSubviews];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"scanQR"])
    {
        QRScannerVC *vc = segue.destinationViewController;
        vc.delegate = self;
    }
}

// *****************************************************************************
#pragma mark -                                          QRScanner delegate
// *****************************************************************************
- (void)qrScannerDidFinish
{
    DLog();
    
    [self dismissViewControllerAnimated:NO completion:Nil];
}

- (void)qrScannerDidCancel
{
    DLog();
    
    [self dismissViewControllerAnimated:YES completion:Nil];
}

// *****************************************************************************
#pragma mark -                                                  Target Actions
// *****************************************************************************
- (IBAction)openQRScanner
{
    [self performSegueWithIdentifier:@"scanQR" sender:self];
}

// *****************************************************************************
#pragma mark -                                                  Private Methods
// *****************************************************************************
- (void)localizeLabels
{
    [self.pressToScanButton setTitle:NSLocalizedString(@"pressToScan", Nil) forState:UIControlStateNormal];
    [self.helpButton setTitle:NSLocalizedString(@"help", Nil) forState:UIControlStateNormal];
}

- (void)inicializarBannerAd
{
    DLog();
    
    //Banner StartApp
    _startAppAd = [[STAStartAppAd alloc] init];
}

- (void)showStartAppBanner
{
    DLog();

    if (_bannerView)
    {
        [_bannerView removeFromSuperview];
        _bannerView = Nil;
    }
    
    _bannerView = [[STABannerView alloc] initWithSize:STA_AutoAdSize
                                           autoOrigin:STAAdOrigin_Bottom
                                             withView:self.container
                                         withDelegate:nil];
    [self.container addSubview:_bannerView];
}

@end
