//
//  QRHelpVC.h
//  QRScanner
//
//  Created by Raúl Blánquez on 25/09/14.
//  Copyright (c) 2014 Raul Blanquez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QRHelpVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *helpTitle;
@property (weak, nonatomic) IBOutlet UITextView *helpBody;

@end
