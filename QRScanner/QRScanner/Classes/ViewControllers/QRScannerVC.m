//
//  QRScannerVC.m
//  QRScanner
//
//  Created by Raúl Blánquez on 22/09/14.
//  Copyright (c) 2014 Raul Blanquez. All rights reserved.
//

#import "QRScannerVC.h"
#import "GlobalInclude.h"
#import "QRManager.h"
#import <StartApp/StartApp.h>
@import AVFoundation;
@import QuartzCore;

@interface QRScannerVC () <AVCaptureMetadataOutputObjectsDelegate, AVAudioPlayerDelegate>
{
    STABannerView   *_bannerView;
    STAStartAppAd   *_startAppAd;
}

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;
@property (nonatomic, strong) AVCaptureMetadataOutput *metadataOutput;
@property (nonatomic, strong) AVCaptureDevice *videoCaptureDevice;
@property (nonatomic, strong) AVCaptureDeviceInput *videoInput;

@property (nonatomic, strong) AVAudioPlayer   *scanFinishSound;

@end

@implementation QRScannerVC

// *****************************************************************************
#pragma mark -                                          ViewController Lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    DLog();
    
    [super viewDidLoad];
    
    [self initData];
    [self localizeLabels];
    [self inicializarBannerAd];
}

- (void)viewWillAppear:(BOOL)animated
{
    DLog();

    [self makeView];
    
    [self.captureSession startRunning];
    
    DLog(@"overlay = %f %f %f %f", self.overlayView.frame.origin.x, self.overlayView.frame.origin.y, self.overlayView.frame.size.height, self.overlayView.frame.size.width);

}

- (void)viewDidLayoutSubviews
{
    DLog();
    
    [super viewDidLayoutSubviews];
    
    //Get Preview Layer connection
    AVCaptureConnection *previewLayerConnection=self.previewLayer.connection;
    
    if ([previewLayerConnection isVideoOrientationSupported])
    {
        UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
        
        switch (deviceOrientation) {
            case UIDeviceOrientationLandscapeLeft:
                previewLayerConnection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
                break;
                
            case UIDeviceOrientationLandscapeRight:
                previewLayerConnection.videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
                break;
                
            case UIDeviceOrientationPortrait:
                previewLayerConnection.videoOrientation = AVCaptureVideoOrientationPortrait;
                break;
                
            case UIDeviceOrientationPortraitUpsideDown:
                previewLayerConnection.videoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
                break;
                
            default:
                break;
        }
    }
}

// *****************************************************************************
#pragma mark -                                          Custom getter & setters
// *****************************************************************************
- (AVCaptureVideoPreviewLayer *)previewLayer
{
    if (!_previewLayer)
    {
        _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
        _previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    }
    
    return _previewLayer;
}

- (AVCaptureSession *)captureSession
{
    if (!_captureSession)
    {
        _captureSession = [[AVCaptureSession alloc] init];
    }
    
    return _captureSession;
}

- (AVCaptureMetadataOutput *)metadataOutput
{
    if (!_metadataOutput)
    {
        _metadataOutput = [[AVCaptureMetadataOutput alloc] init];
    }
    
    return _metadataOutput;
}

- (AVCaptureDevice *)videoCaptureDevice
{
    if (!_videoCaptureDevice)
    {
        _videoCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        NSError *error = Nil;
        [_videoCaptureDevice lockForConfiguration:&error];
        [_videoCaptureDevice setFocusPointOfInterest:CGPointMake(0.5, 0.5)];
        [_videoCaptureDevice setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
        [_videoCaptureDevice unlockForConfiguration];
    }
    
    return _videoCaptureDevice;
}

- (AVCaptureDeviceInput *)videoInput
{
    if (!_videoInput)
    {
        NSError *error = nil;
        _videoInput = [AVCaptureDeviceInput deviceInputWithDevice:self.videoCaptureDevice error:&error];
        
        if (error)
        {
            DLog(@"Error: %@", error);
        }
    }
    
    return _videoInput;
}

// *****************************************************************************
#pragma mark -                            AVCaptureMetadataOutputObjectsDelegate
// *****************************************************************************
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    DLog();
    
    for(AVMetadataObject *metadataObject in metadataObjects)
    {
        AVMetadataMachineReadableCodeObject *readableObject = (AVMetadataMachineReadableCodeObject *)metadataObject;
        if([metadataObject.type isEqualToString:AVMetadataObjectTypeQRCode])
        {
            [self.captureSession stopRunning];
            [self.scanFinishSound play];
            [QRManager manageQRCode:readableObject.stringValue];
        }
        else if ([metadataObject.type isEqualToString:AVMetadataObjectTypeEAN13Code])
        {
            DLog(@"EAN 13 = %@", readableObject.stringValue);
        }
    }
    
    [self.delegate qrScannerDidFinish];
}

// *****************************************************************************
#pragma mark -                                                  Target Actions
// *****************************************************************************
- (IBAction)cancelScan
{
    [self.delegate qrScannerDidCancel];
}

// *****************************************************************************
#pragma mark -                                                  Private Methods
// *****************************************************************************
- (void)initData
{
    DLog();
    
    if (self.videoInput)
    {
        [self.captureSession addInput:self.videoInput];
    }
    
    //CGRect visRect = [self.previewLayer metadataOutputRectOfInterestForRect:self.overlayView.layer.frame];
    //self.metadataOutput.rectOfInterest = visRect;

    [self.captureSession addOutput:self.metadataOutput];
    [self.metadataOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [self.metadataOutput setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode, AVMetadataObjectTypeEAN13Code]];
    
    NSURL *soundURL        = [[NSBundle mainBundle] URLForResource:@"beep" withExtension:@"wav"];
    self.scanFinishSound  = [[AVAudioPlayer alloc] initWithContentsOfURL:soundURL error:nil];
    self.scanFinishSound.delegate = self;
    [self.scanFinishSound prepareToPlay];
}

- (void)makeView
{
    DLog();
    
    self.previewLayer.frame = self.view.layer.frame;
    
    [self.view.layer addSublayer:self.previewLayer];
    
    self.overlayView.layer.borderWidth = 5;
    self.overlayView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.overlayView.layer.cornerRadius = 10.0;
    self.overlayView.alpha = 0.8;
    
    self.shadowLayer.layer.borderWidth = self.overlayView.layer.borderWidth;
    self.shadowLayer.layer.borderColor = [UIColor blackColor].CGColor;
    self.shadowLayer.layer.cornerRadius = 10.0;
    self.overlayView.alpha = 0.8;
    
    [self.view bringSubviewToFront:self.shadowLayer];
    [self.view bringSubviewToFront:self.overlayView];
    [self.view bringSubviewToFront:self.cancelButton];

    [self.view bringSubviewToFront:self.container];
}

- (void)localizeLabels
{
    [self.cancelButton setTitle:NSLocalizedString(@"cancel", Nil) forState:UIControlStateNormal];
}

- (void)inicializarBannerAd
{
    DLog();
    
    //Banner StartApp
    _startAppAd = [[STAStartAppAd alloc] init];    
}

- (void)showStartAppBanner
{
    DLog();
    
    if (_bannerView)
    {
        [_bannerView removeFromSuperview];
        _bannerView = Nil;
    }
    
    _bannerView = [[STABannerView alloc] initWithSize:STA_AutoAdSize
                                           autoOrigin:STAAdOrigin_Bottom
                                             withView:self.container
                                         withDelegate:nil];
    [self.container addSubview:_bannerView];
}


@end
