//
//  AppDelegate.h
//  QRScanner
//
//  Created by Raúl Blánquez on 22/09/14.
//  Copyright (c) 2014 Raul Blanquez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

