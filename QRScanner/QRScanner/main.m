//
//  main.m
//  QRScanner
//
//  Created by Raúl Blánquez on 22/09/14.
//  Copyright (c) 2014 Raul Blanquez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
